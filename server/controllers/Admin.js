'use strict';

var utils = require('../utils/writer.js');
var Admin = require('../service/AdminService');

module.exports.getAllUsers = function getAllUsers (req, res, next) {
  Admin.getAllUsers()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getDataUser = function getDataUser (req, res, next) {
  Admin.getDataUser()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateRoleUser = function updateRoleUser (req, res, next, body) {
  Admin.updateRoleUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
