'use strict';

var utils = require('../utils/writer.js');
var Room = require('../service/RoomService');

module.exports.chooseOption = function chooseOption (req, res, next, body) {
  Room.chooseOption(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createRoom = function createRoom (req, res, next, body) {
  Room.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteRoom = function deleteRoom (req, res, next) {
  Room.deleteRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editNameRoom = function editNameRoom (req, res, next, body) {
  Room.editNameRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRooms = function getRooms (req, res, next) {
  Room.getRooms()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getSpesificRoom = function getSpesificRoom (req, res, next) {
  Room.getSpesificRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
