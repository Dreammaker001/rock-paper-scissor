'use strict';

var utils = require('../utils/writer.js');
var Player = require('../service/PlayerService');

module.exports.getAllPlayer = function getAllPlayer (req, res, next, point_min, point_max, level) {
  Player.getAllPlayer(point_min, point_max, level)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllRoomsPlayer = function getAllRoomsPlayer (req, res, next) {
  Player.getAllRoomsPlayer()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPlayerBio = function getPlayerBio (req, res, next) {
  Player.getPlayerBio()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sendInvitation = function sendInvitation (req, res, next) {
  Player.sendInvitation()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updatePlayerBio = function updatePlayerBio (req, res, next) {
  Player.updatePlayerBio()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
