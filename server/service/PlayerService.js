'use strict';


/**
 * Get all player
 *
 * point_min Integer The minimum number of points you want to find (optional)
 * point_max Integer The maximum number of points you want to find (optional)
 * level String The level of the player you want to find (optional)
 * returns inline_response_200_2
 **/
exports.getAllPlayer = function(point_min,point_max,level) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "username" : "udin",
    "point" : 1200,
    "level" : "Class D",
    "avatar_url" : "/assets/profile/46452.jpg"
  }, {
    "id" : 2,
    "username" : "budi",
    "point" : 100,
    "level" : "NOVICE",
    "avatar_url" : "/assets/profile/65452.jpg"
  }, {
    "id" : 3,
    "ussername" : "nurhadi",
    "point" : 1400,
    "level" : "Class C",
    "avatar_url" : "/assets/profile/54545.jpg"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get all game room specific player
 *
 * returns inline_response_200_4
 **/
exports.getAllRoomsPlayer = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "name" : "Gabung kuy",
    "state" : "Opened",
    "turn" : "Player 1 turn"
  }, {
    "id" : 2,
    "name" : "Bermain bersama",
    "state" : "Opened",
    "turn" : "FINISHED"
  }, {
    "id" : 3,
    "name" : "Maju Tak gentar",
    "state" : "Opened",
    "turn" : "FINISHED"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Retrieve the bio of a specific player
 *
 * returns inline_response_200_3
 **/
exports.getPlayerBio = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "address" : "Jl. Bersama dia No.362, Siti Rejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20144",
    "avatar_url" : "/assets/profile/671bjhkasdkb.jpg",
    "user_id" : 2,
    "bio" : "aku ganteng sekali",
    "phone_number" : "085612483322",
    "id" : 1
  },
  "message" : "user bio",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * invitation to join the game
 *
 * returns inline_response_200_5
 **/
exports.sendInvitation = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "from_user_name" : "udin",
  "to_user_id" : 2,
  "to_user_name" : "bagas",
  "from_user_level" : "NOVICE",
  "from_user_id" : 1
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * update player bio
 *
 * returns inline_response_200_3
 **/
exports.updatePlayerBio = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "address" : "Jl. Bersama dia No.362, Siti Rejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20144",
    "avatar_url" : "/assets/profile/671bjhkasdkb.jpg",
    "user_id" : 2,
    "bio" : "aku ganteng sekali",
    "phone_number" : "085612483322",
    "id" : 1
  },
  "message" : "user bio",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

