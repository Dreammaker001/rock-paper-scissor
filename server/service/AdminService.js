'use strict';


/**
 * endpoint to fetch all user data
 *
 * returns inline_response_200_6
 **/
exports.getAllUsers = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "username" : "udin",
    "point" : 1200,
    "level" : "Class D",
    "role_id" : 1,
    "role_name" : "Admin",
    "bio_id" : 1
  }, {
    "id" : 2,
    "username" : "budi",
    "point" : 100,
    "level" : "NOVICE",
    "role_id" : 2,
    "role_name" : "Player",
    "bio_id" : 2
  }, {
    "id" : 3,
    "ussername" : "nurhadi",
    "point" : 1400,
    "level" : "Class C",
    "role_id" : 2,
    "role_name" : "Player",
    "bio_id" : 3
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Endpoint for retrieving specific user data
 *
 * returns inline_response_200_7
 **/
exports.getDataUser = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "role_name" : "Admin",
    "address" : "Jl. Bersama dia No.362, Siti Rejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20144",
    "avatar_url" : "/assets/profile/671bjhkasdkb.jpg",
    "user_id" : 2,
    "role_id" : 1,
    "user_name" : "udin",
    "bio" : "aku ganteng sekali",
    "phone_number" : "085612483322",
    "id" : 1
  },
  "message" : "user bio",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Endpoint for retrieving specific user data
 * # Update role user  Use this endpoint to update the user role 
 *
 * body User_id_body 
 * returns inline_response_200_7
 **/
exports.updateRoleUser = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "role_name" : "Admin",
    "address" : "Jl. Bersama dia No.362, Siti Rejo I, Kec. Medan Kota, Kota Medan, Sumatera Utara 20144",
    "avatar_url" : "/assets/profile/671bjhkasdkb.jpg",
    "user_id" : 2,
    "role_id" : 1,
    "user_name" : "udin",
    "bio" : "aku ganteng sekali",
    "phone_number" : "085612483322",
    "id" : 1
  },
  "message" : "user bio",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

