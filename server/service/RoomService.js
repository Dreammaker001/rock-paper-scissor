'use strict';


/**
 * To choose rock, scissors, or paper, in a round
 * # Choose rock, scissors, or paper Use this endpoint to play by selecting rock, scissors, or paper, in a round in a room in which he is playing. 
 *
 * body Id_round_body 
 * returns inline_response_201_2
 **/
exports.chooseOption = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "room_name" : "Gabung kuy",
    "turn" : "PLAYER 2 TURN",
    "rounds" : [ "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]", "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]" ]
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Create a new game room
 * # Create a new game room  Use this endpoint to create a new game room for this site. 
 *
 * body Object 
 * returns inline_response_201_2
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "room_name" : "Gabung kuy",
    "turn" : "PLAYER 2 TURN",
    "rounds" : [ "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]", "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]" ]
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete Game Room
 * # Delete game room  Use this endpoint to delete game space for this site. 
 *
 * returns inline_response_200_1
 **/
exports.deleteRoom = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "message" : "Successfully deleted the game room",
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Edit game room name
 * # End point for changing game room name
 *
 * body Object 
 * returns inline_response_201_2
 **/
exports.editNameRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "room_name" : "Gabung kuy",
    "turn" : "PLAYER 2 TURN",
    "rounds" : [ "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]", "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]" ]
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Retrieve all open game room data
 * # Get data rooms for Player and Admin Only retrieve game room data with open state (for Player) but specifically for admin can also retrieve closed room status 
 *
 * returns inline_response_200
 **/
exports.getRooms = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : [ {
    "id" : 1,
    "name" : "Gabung kuy",
    "state" : "Opened"
  }, {
    "id" : 2,
    "name" : "Bermain bersama",
    "state" : "Opened"
  }, {
    "id" : 3,
    "name" : "Maju Tak gentar",
    "state" : "Opened"
  } ],
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get specific game room
 * # Join the game room  Use this endpoint to join the game room for this site. 
 *
 * returns inline_response_201_2
 **/
exports.getSpesificRoom = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "room_id" : 1,
    "room_name" : "Gabung kuy",
    "turn" : "PLAYER 2 TURN",
    "rounds" : [ "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]", "[{\"round_id\":1,\"status\":\"FINISHED\",\"winner_id\":1,\"user_id\":1,\"game_option_id\":1,\"options\":\"rock\"},{\"round_id\":2,\"status\":\"PLAYER_1_TURN\",\"user_id\":2,\"game_option_id\":2,\"options\":\"paper\"}]" ]
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

