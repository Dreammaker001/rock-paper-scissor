import { NavLink } from "react-router-dom"

import "../Styles/Dashboard.css"

function buttonActive (id) {
    let x = document.getElementById(id)
    // console.log(x.style.backgroundColor)
    if (x.style.backgroundColor === "rgb(255, 193, 7)") {
        x.style.backgroundColor = 'rgb(33, 35, 37)'
    }
    else{
        x.style.backgroundColor = "rgb(255, 193, 7)"
    }
}
function Catgeory() {

    const category = ['novice', 'class a', 'class b', 'class c', 'class d', 'candidate master', 'grand master']
    return (
        <div className='category text-white container py-5'>
            <h3 className="fw-bold pb-2">CHOOSE YOUR OPPONENT</h3>
            <div className='d-flex justify-content-between'>
                {category.map((i, index) => <NavLink to={'/'+index} id={index} className="btn fw-bold py-4 px-4 text-uppercase text-white"
                onClick={(e)=>{
                    e.preventDefault() 
                    buttonActive(index)
                }}>{i}</NavLink>)}
            </div>
        </div>
    )
}

export default Catgeory