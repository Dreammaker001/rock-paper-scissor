import { Link } from "react-router-dom"
import {Dropdown} from 'react-bootstrap'
import {useSelector, useDispatch} from 'react-redux'
import { useEffect, useState } from "react"

function Navbar () {
    const dispatch = useDispatch()
    let user = useSelector(state => state.login)
    let [isLogin, setLogin] = useState(false)
    useEffect(() => {
        !!user.isLogin ? setLogin(true) : setLogin(false)
        // console.log("user.isLogin : "+user.isLogin)
    }, [user])
    return (
        <div className="bg-nav">
            <nav className="navbar navbar-expand-lg navbar-light justify-content-between container">
                <Link to="/" className="navbar-brand text-white fw-bold">SUIT GAME</Link>
                
                {
                    !!isLogin ? (
                        <Dropdown>
                            <Dropdown.Toggle variant="" className="text-white fw-bold" id="dropdown-basic">
                                {user.name}
                            </Dropdown.Toggle>

                            <Dropdown.Menu className="bg-gray">
                                <Dropdown.Item to="" className="text-white" onClick={() => dispatch({type: 'LOGOUT'})}>Logout</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    ) : (
                        <div className="d-flex">
                            <Link to="/register" className="nav-link text-white">REGISTER</Link>
                            <Link to="/login" className="nav-link text-white">LOGIN</Link>
                        </div>
                    )
                }
                
            </nav>
        </div>
    )
}

export default Navbar