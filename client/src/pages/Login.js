import React, { useMemo, useState } from 'react';
import "../Styles/Login.sass";
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

const Login = () =>{
    let [username, setUsername] = useState()
    let [password, setPassword] = useState()
    let user = useSelector(state => state.login)
    let [message, setMessage] = useState('')
    const navigate = useNavigate()
    const dispatch = useDispatch()
    let [submit, setSubmit] = useState(false)

    useMemo(()=>{
        // console.log("efek")
        // console.log(user.isLogin)
        if(!!user.isLogin){
            setMessage("")
            navigate('/')
        } else if (!!submit && !(!!user.isLogin)){
            setMessage("Invalid username or password")
        }
    }, [user])

    return(
        <div className="row m-0 h-100">
            <form className='form-login border border-white mx-auto my-auto'>
                <h2 className="text-center mb-4 fw-bold">Login</h2>
                <p  className='text-danger'>{message}</p>
                <div className="form-floating mb-3">
                <input type="text" className="form-control" id="floatingInput" placeholder="Username"
                onChange={(e) => setUsername(e.target.value)}/>
                    <label for="floatingInput">Insert your username</label>
                </div>
                <div class="form-floating">
                <input type="password" className="form-control" id="floatingPassword" placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}/>
                    <label for="floatingPassword">Insert your password</label>
                </div>
                <button type="submit" className="btn btn-warning w-100 text-white mt-3 fw-bold"
                    onClick={(e) =>{
                        e.preventDefault()
                        setSubmit(true)
                        dispatch({type: 'LOGIN', username: username, password: password})
                        }}>Login</button>
            </form>
        </div>
    )
}

const validate = (username, password, conPassword) => {
    let check = /^[a-z\d\-_\S]+$/i.test(username)
    // console.log(username)
    // console.log(password)
    if(!username || !password) return "Username and password cannot be empty"
    if(!(username.length >= 6 && username.length <= 20 && check)) return "Username must be alphanumeric without spaces, minimum 6 characters, maximum 20 characters."
    if(password !== conPassword || password.length < 6) return "Password is at least 6 characters and must be the same between password and confirmation"
    return false
}

const Register = () =>{
    let [username, setUsername] = useState()
    let [password, setPassword] = useState()
    let [conPassword, setconPassword] = useState()
    let [message, setMessage] = useState('')
    const navigate = useNavigate()
    const dispatch = useDispatch()

    return(
        <div className="row m-0 h-100">
            <form className='form-login border border-white mx-auto my-auto'>
                <h2 className="text-center mb-4 fw-bold">Register</h2>
                <p className='text-danger'>{message}</p>
                <div class="form-floating mb-3">
                <input type="text" className="form-control" id="floatingInput" placeholder="Username"
                onChange={(e) => setUsername(e.target.value)}/>
                    <label for="floatingInput">Insert your username</label>
                </div>
                <div class="form-floating">
                <input type="password" className="form-control" id="floatingPassword" placeholder="Password"
                 onChange={(e) => setPassword(e.target.value)}/>
                    <label for="floatingPassword">Insert your password</label>
                </div>
                <div class="form-floating mt-3">
                <input type="password" className="form-control" id="floatingPasswordConfirm" placeholder="Password Confirm"
                onChange={(e) => setconPassword(e.target.value)}/>
                    <label for="floatingPasswordConfirm">Confirm your password</label>
                </div>
                <button type="submit" className="btn btn-warning w-100 text-white mt-3 fw-bold"
                    onClick={(e) =>{
                        e.preventDefault()
                        let check = validate(username, password, conPassword)
                        if(!!check) return setMessage(check)
                        dispatch({type: 'REGISTER', username: username, password: password})
                        navigate('/login')
                }}>Register</button>
            </form>
        </div>
    )
}
export {Login, Register};