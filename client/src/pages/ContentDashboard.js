import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
// import Evan from "../assets/Evan.png"
import dataUser from "../assets/opponents.json"

function convertDate (value) {
    const newDate = new Date(value).toDateString()
    return newDate
}

function buttonActive (id) {
    let x = document.getElementById(id)
    // console.log(x.style.backgroundColor)
    if (x.style.backgroundColor === "rgb(255, 181, 69)") {
        x.style.backgroundColor = 'rgb(33, 35, 37)'
    }
    else{
        x.style.backgroundColor = "rgb(255, 181, 69)"
    }
}


function ContentDashboard () {
    let [arr, setArr] = useState(["GRAND MASTER"]);
    let [data, setData] = useState(dataUser);
    const category = ['NOVICE', 'CLASS A', 'CLASS B', 'CLASS C', 'CLASS D', 'CANDIDATE MASTER', 'GRAND MASTER']
    
    useEffect(() => {
        setData(dataUser.filter(item => arr.includes(item.level)))
        // console.log("array use EF")
        // console.log(arr)
    }, [arr])

    // useEffect(() => {
    //     console.log("ini dari arr data")
    //     console.log(data)
    // }, [data])

    return (
        <div className="container">
            <div className='category text-white py-5'>
                <h3 className="fw-bold pb-2">CHOOSE YOUR OPPONENT</h3>
                <div className='d-flex justify-content-between flex-wrap'>
                    {category.map((i, index) => <Link to="#" id={index} key={index} className="btn fw-bold py-4 px-4 text-white"
                    style={index ===6 ? {backgroundColor: 'rgb(255, 181, 69)'}: {}}
                    onClick={(e)=>{
                        e.preventDefault()
                        const arrIndex = arr.indexOf(i)
                        if(arrIndex !== -1){
                            setArr(arr.filter(item => item !== i))
                        } else{
                            setArr([...arr, i])
                            console.log("terpanggil")
                        }
                        buttonActive(index)
                    }}>{i}</Link>)}
                </div>
            </div>
            <div className="d-flex flex-wrap justify-content-between">
                { 
                data.map((i, index) => { return (
                <div className="card bg-gray mb-5" style={{width: '18rem'}} key={index}>
                    <div className="card-header d-flex">
                        <div className="me-2">
                            <div className="bg-yellow rounded-circle">
                                <img className="rounded-circle" src={i.avatar} style={{margin: '0 7px 5px 0', width: '67px', height: '66px'}} alt={i.name}/>
                            </div>
                        </div>
                        <div className="align-self-center">
                            <h5 className="card-title yellow">{i.name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">{i.level}</h6>
                        </div>
                    </div>
                    <div className="card-body text-white">
                        <p className="card-text">{i.bio}</p>
                        <h6 className="text-muted">{convertDate(i.createdAt)}</h6>
                        <Link to="#" className="btn bg-yellow card-link rounded-pill text-white px-4 fw-bold">FIGHT</Link>
                    </div>
                </div>
                )})
                }
            </div>
        </div>
    )
}

export default ContentDashboard