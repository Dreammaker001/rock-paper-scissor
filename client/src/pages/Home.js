import React from "react";
import "bootstrap/dist/css/bootstrap.min.css"
import {Login, Register}from "./Login";
import "../Styles/Global.scss"
import {Routes, Route } from "react-router-dom";
import Dashboard from "./Dashboard";

import store from "../app/store";
import {Provider} from 'react-redux'

function Home () {
    return (
        <Provider store={store}>
            <Routes>
                <Route path='/' element={<Dashboard/>}/>
                <Route path='/login' element={<Login/>}/>
                <Route path='/register' element={<Register/>}/>
            </Routes>
        </Provider>
    )
}
export default Home