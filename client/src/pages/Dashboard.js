// import Catgeory from "../components/Category"
import Navbar from "../components/Navbar"
import ContentDashboard from "./ContentDashboard"

import "../Styles/Dashboard.sass"
function Dashboard () {
    return (
        <>
            <Navbar/>
            <ContentDashboard/>
        </>
    )
}

export default Dashboard