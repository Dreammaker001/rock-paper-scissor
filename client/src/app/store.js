import {combineReducers, createStore} from 'redux'
import loginReducer from "../features/Login/reducer";

let rootReducers = combineReducers({
    login: loginReducer
})

let store = createStore(rootReducers)

export default store